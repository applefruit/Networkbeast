﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace NetworkBeast.NetworkSending
{
    class NetworkSender
    {
        private static NetworkSendingQueue m_Queue;

        public NetworkSender(NetworkSendingQueue queue)
        {
            LogDebug.LoadDebug(this);
            m_Queue = queue;
        }

        public static void SendToOne(ByteBuffer buffer, Socket socket)
        {
            SendingData data = new SendingData
            {
                Buffer = buffer,
                Receiver = socket,
                Type = PacketType.SendToOne
            };

            m_Queue.QueuePackage(buffer.ToArray(), socket , data.Type);
        }

        public static void SendToAll(ByteBuffer buffer)
        {
            SendingData data = new SendingData
            {
                Buffer = buffer,
                Type = PacketType.SendToAll
            };

            m_Queue.QueuePackage(buffer.ToArray(), null, data.Type);
        }

        public static void SendToAllExcept(ByteBuffer buffer, Socket exception) 
        {
            SendingData data = new SendingData
            {
                Buffer = buffer,
                Receiver = exception,
                Type = PacketType.SendToAllExcept
            };

            m_Queue.QueuePackage(buffer.ToArray(), exception, data.Type);
        }
    }
}
