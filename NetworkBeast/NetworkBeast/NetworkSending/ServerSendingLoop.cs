﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using NetworkBeast.Network;

namespace NetworkBeast.NetworkSending
{
    class ServerSendingLoop
    {
        private readonly NetworkSendingQueue m_SendingQueue;
        private readonly Server m_Server;
        private readonly bool m_IsSending;

        public ServerSendingLoop(Server server,NetworkSendingQueue queue)
        {
            LogDebug.LoadDebug(this);
            m_SendingQueue = queue;
            m_IsSending = true;
            m_Server = server;

            Thread sendingThreadOne = new Thread(Coreloop);
            sendingThreadOne.Start();

        }


        private void Coreloop()
        {          
            while (m_IsSending)
            {
                if (m_SendingQueue.HasPackets())
                {

                    SendingData sendableData = m_SendingQueue.SendingQueue.Dequeue();
                    byte[] bytes = sendableData.Buffer.ToArray();
                    int length = bytes.Length;

                    switch (sendableData.Type)
                    {
                        case PacketType.SendToOne:

                            sendableData.Receiver.BeginSend(bytes, 0,
                                length,
                                SocketFlags.None,
                                new AsyncCallback(SendCallBack), null);

                            break;
                        case PacketType.SendToAll:

                            foreach (Client c in m_Server.ConnectedClients.Values)
                            {
                                c.Socket.BeginSend(bytes, 0,
                                    length,
                                    SocketFlags.None,
                                    new AsyncCallback(SendCallBack), null);
                            }

                            break;
                        case PacketType.SendToAllExcept:
                            //If more performance needed from LINQ
                            foreach (Client c in m_Server.ConnectedClients.Values.Where(x =>
                                x.Socket != sendableData.Receiver))
                            {
                                c.Socket.BeginSend(bytes, 0,
                                    length,
                                    SocketFlags.None,
                                    new AsyncCallback(SendCallBack), null);
                            }

                            break;

                    }
                }
            }
        }

        private void SendCallBack(IAsyncResult ar)
        {
        }
    }
}
