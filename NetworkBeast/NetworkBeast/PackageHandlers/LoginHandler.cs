﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using NetworkBeast.GameLogic;
using NetworkBeast.Network;
using NetworkBeast.NetworkSending;

namespace NetworkBeast.PackageHandlers
{
    class LoginHandler
    {
        private Random m_Rnd;
        public static LoginHandler Instance;

        internal LoginHandler()
        {
            m_Rnd = new Random();
            Instance = this;
        }

        //Handles login package
        public void HandleLogin(ClientNetworkPackage package)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(package.Data);
            int size = buffer.ReadInt();
            int id = buffer.ReadInt();
            string username = buffer.ReadString();
            string password = buffer.ReadString();

            SendAckPackage(package.Socket);
            
            LogDebug.DebugMessage(" - Accoutname: - " + username, LogDebug.MessageType.PacketDebug);
        }

        private void SendAckPackage(Socket s)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ServerPacket.LoginAck);
            NetworkSender.SendToOne(buffer, s);
        }


    }
}
