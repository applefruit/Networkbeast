﻿using System;
using System.Collections.Generic;
using System.Text;
using NetworkBeast.Network;
using NetworkBeast.NetworkSending;

namespace NetworkBeast.GameLogic
{
    class VerificationLogic
    {
        public void SetClientData(Client client)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ServerPacket.Verification);
            string time = DateTime.UtcNow.Millisecond.ToString();
            string hash = CreateHash(time);
            buffer.WriteString(hash);
            client.HashToken = hash;

            LogDebug.DebugMessage("A new hash has been created: " + hash, LogDebug.MessageType.Info);

            NetworkSender.SendToOne(buffer, client.Socket);
        }

        public static string CreateHash(string input)
        {
            string salt = input = input + "243s";
            string hashCode = String.Format("{0:X}", input.GetHashCode());

            return hashCode;
        }
    }
}
