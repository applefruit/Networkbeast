﻿namespace NetworkBeastTypes
{
    public struct Vector2
    {
        public static Vector2 zero
        {
            get { return new Vector2(0, 0); }
        }

        private int m_X;

        public int X
        {
            get { return m_X; }
            set { m_X = value; }
        }

        private int m_Y;

        public int Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }

        public Vector2(int x, int y)
        {
            m_X = x;
            m_Y = y;
        }

        public static Vector2 operator +(Vector2 vec1, Vector2 vec2)
        {
            return new Vector2(vec1.X + vec2.X, vec1.Y + vec2.Y);
        }

        public static Vector2 operator -(Vector2 vec1, Vector2 vec2)
        {
            return new Vector2(vec1.X - vec2.X, vec1.Y - vec2.Y);
        }
    }

    public struct Vector2f
    {
        public static Vector2f zero
        {
            get { return new Vector2f(0.0f, 0.0f); }
        }

        private float m_X;

        public float X
        {
            get { return m_X; }
            set { m_X = value; }
        }

        private float m_Y;

        public float Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }

        public Vector2f(float x, float y)
        {
            m_X = x;
            m_Y = y;
        }

        public static Vector2f operator +(Vector2f vec1, Vector2f vec2)
        {
            return new Vector2f(vec1.X + vec2.X, vec1.Y + vec2.Y);
        }

        public static Vector2f operator -(Vector2f vec1, Vector2f vec2)
        {
            return new Vector2f(vec1.X - vec2.X, vec1.Y - vec2.Y);
        }
    }

    public struct Vector3
    {
        public static Vector3 zero
        {
            get { return new Vector3(0, 0, 0); }
        }

        public static Vector3 One
        {
            get { return new Vector3(1, 1, 1); }
        }

        private int m_X;

        public int X
        {
            get { return m_X; }
            set { m_X = value; }
        }

        private int m_Y;

        public int Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }

        private int m_Z;

        public int Z
        {
            get { return m_Z; }
            set { m_Z = value; }
        }

        public Vector3(int x, int y, int z)
        {
            m_X = x;
            m_Y = y;
            m_Z = z;
        }

        public static Vector3 operator +(Vector3 vec1, Vector3 vec2)
        {
            return new Vector3(vec1.X + vec2.X, vec1.Y + vec2.Y, vec1.Z + vec2.Z);
        }

        public static Vector3 operator -(Vector3 vec1, Vector3 vec2)
        {
            return new Vector3(vec1.X - vec2.X, vec1.Y - vec2.Y, vec1.Z - vec2.Z);
        }
    }

    public struct Vector3f
    {
        public static Vector3f zero
        {
            get { return new Vector3f(0, 0, 0); }
        }

        public static Vector3f One
        {
            get { return new Vector3f(1f, 1f, 1f); }
        }

        private float m_X;

        public float X
        {
            get { return m_X; }
            set { m_X = value; }
        }

        private float m_Y;

        public float Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }

        private float m_Z;

        public float Z
        {
            get { return m_Z; }
            set { m_Z = value; }
        }

        public Vector3f(float x, float y, float z)
        {
            m_X = x;
            m_Y = y;
            m_Z = z;
        }

        public static Vector3f operator +(Vector3f vec1, Vector3f vec2)
        {
            return new Vector3f(vec1.X + vec2.X, vec1.Y + vec2.Y, vec1.Z + vec2.Z);
        }

        public static Vector3f operator -(Vector3f vec1, Vector3f vec2)
        {
            return new Vector3f(vec1.X - vec2.X, vec1.Y - vec2.Y, vec1.Z - vec2.Z);
        }
    }
}