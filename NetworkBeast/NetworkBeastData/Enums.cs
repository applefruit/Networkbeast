﻿public enum ServerPacket
{
    Debug = 1,
    Verification,
    Login,
    LoginAck,
    Instantiate,
}

public enum ClientPacket
{
    Debug = 1,
    Verification,
    Login,
    Instantiate
}

public enum PacketType
{
    SendToOne,
    SendToAll,
    SendToAllExcept
}

public enum TestEnum
{
    hoi
}