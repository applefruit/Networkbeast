﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NetworkBeast
{
    public class PackageHandling
    {
        private Queue<ClientNetworkPackage> m_Packages;

        public Queue<ClientNetworkPackage> PackageQueue
        {
            get => m_Packages;
            set => m_Packages = value;
        }

        public PackageHandling()
        {
            LogDebug.LoadDebug(this);
            m_Packages = new Queue<ClientNetworkPackage>();
        }

        public void QueuePackage(byte[] data, Socket socket)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            int packerSize = buffer.ReadInt();
            int packetId = buffer.ReadInt();

            PackageQueue.Enqueue(new ClientNetworkPackage((ClientPacket)packetId, socket, data));
        }

        public bool HasPackets()
        {
            if (m_Packages.Count > 0)
                return true;
            else return false;
        }
    }
}
